import { Body, Controller, Get, Post } from '@nestjs/common';
//import { User } from './users.entity';

import { User } from '@nestjsplus/nestjs-package-starter/dist/output/entities/user';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService){}
    @Get()
    async findAll(){
        return await this.usersService.findAll();
    }
    @Post()
    async create(@Body() userData: User): Promise<any> {
      return await this.usersService.create(userData);
    } 
}
