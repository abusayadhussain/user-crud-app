import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '@nestjsplus/nestjs-package-starter';
//import { userEntity } from '@nestjsplus/nestjs-package-starter';
import { Repository } from 'typeorm';


@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User)
        private usersRepository: Repository<User>,
    ){}


    async findAll(): Promise<User[]>{
        return await this.usersRepository.find();
    }
    async  create(user: User): Promise<User> {
        return await this.usersRepository.save(user);
    }


}
