import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '@nestjsplus/nestjs-package-starter';
// import { User } from '@nestjsplus/nestjs-package-starter/dist/output/entities/User';
import { UsersController } from './users.controller';
//import { User } from './users.entity';
import { UsersService } from './users.service';
//import { userEntity } from '@nestjsplus/nestjs-package-starter';
@Module({
  imports:[TypeOrmModule.forFeature([User])],
  controllers: [UsersController],
  providers: [UsersService]
})
//userModule() 

export class UsersModule {}
