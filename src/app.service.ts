import { Injectable } from '@nestjs/common';
import { getHello} from '@nestjsplus/nestjs-package-starter'
@Injectable()
export class AppService {
  getHello(): string {
    return getHello();
  }
}
