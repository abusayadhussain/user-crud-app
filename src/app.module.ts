import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {databaseHandle} from '@nestjsplus/nestjs-package-starter';
import { UsersModule } from './users/users.module';

 console.log(databaseHandle());
@Module({
  imports: [ databaseHandle(),UsersModule],
  controllers: [AppController],
  providers: [AppService],
})

export class AppModule {
}
